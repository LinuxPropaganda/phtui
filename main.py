#! /bin/python3

from os import system
from sys import argv
from pornhub_api import PornhubApi

from googletrans import Translator, constants 
#    💙💜🤍💜💙

'''
import youtube_dl
  

def download(url):

    #FFMPEG_OPTIONS = {'before_options': '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5', 'options': '-vn'}
    with youtube_dl.YoutubeDL({"format":"bestaudio"}) as ydl:
        info = ydl.extract_info(url, download = 0)
        
        toremove=[]
        [toremove.append(i) for i in info["formats"] if i["filesize"] is None]
        url2 = info['formats'][0]['url']
    
    return url2
'''

API = PornhubApi()
translator = Translator()


HELP = '''

usage:
    phtui <search prompt> interval-len <len> max-len <len>

interval-len is optional, change <len> to the amount of titles you wish to be displayed per chunk (default 5)
max-len      is optional, change <len> to the amount of titles you wish to be loaded              (default 50)

'''



def main():
    try:
        title()
        ilen, mlen, query = parse_args()
        videos = load_stuff(ilen, mlen, query)
        selection(videos, ilen)
        
    except Exception as e:
        error(f'Shit! I fucked up something! Sorry, it\'s so embarrassing!\nThis thing happend: {e}\nPlease contact me if you have a solution or just to report the bug :)')



def error(cringe):
    print('\33[0;31mERROR:',cringe)
    exit(1)

def place(status):
    print('\33[1;32m'+status+'\33[0;37m]')

def debug(task):
    print('\33[0;37m[ ]', task, end='\r[')


def title():
    print("\33[1;31m+---------------------------------------------+")
    print("|                \33[1;37mPorn\33[0;43;30mHub\33[0;1;31m TUI                  |")
    print("+---------------------------------------------+\33[0;37m")
    print('Enjoy porn without running spyware!\nNow with translations!')




def parse_args():

    if 'help' in argv:
        print(HELP)
        exit(0)

    INTERVAL_LEN = 5
    if 'interval-len' in argv:
        try:
            INTERVAL_LEN = argv[argv.index('interval-len')+1]
            argv.remove('interval-len')
            argv.remove(INTERVAL_LEN)
            INTERVAL_LEN = int(INTERVAL_LEN)
        except: error('interval-len needs a numerical value')
    
    MAX_LEN = 50
    if 'max-len' in argv:
        try:
            MAX_LEN = argv[argv.index('max-len')+1]
            argv.remove('max-len')
            argv.remove(MAX_LEN)
            MAX_LEN = int(MAX_LEN)
        except: error('max-len needs a numerical value')

    if len(argv)<2:
        print('\n(no search argument provided)')
        QUERY = input('\33[1mSEARCH: \33[0;37m')
        print('\n')
    else:
        QUERY = ''.join(argv[1:])
    
    
    return INTERVAL_LEN, MAX_LEN, QUERY


def load_stuff(INTERVAL_LEN, MAX_LEN, QUERY):
    debug('retrieving video info...')


    videos = []
    
    data = API.search.search(
        QUERY,
        ordering="mostviewed",
        period="weekly",    )

    for vid in data.videos:
        #print(dir(vid))
        videos .append({
            "title": translator.translate(vid.title).text,
            "tags": [i.tag_name for i in vid.tags]+[i.category for i in vid.categories],
            "url": vid.url
        })


    place('OK')
    print('-'*30)

    return videos


def selection(videos, INTERVAL_LEN):

    # display search results
    interval = (0, INTERVAL_LEN)
    a=''
    while type(a) != int:
        for i, video in enumerate(videos[interval[0]:interval[1]]):
            print('\33[0;32m[\33[1;32m',i+interval[0],'\33[0;32m]\33[1;37m ', video["title"], end='\n')

        print('\33[0;37m[ ] ENTER to display more results, Q to quit, number to play video', end='\r[')
        a = input()
        print('                                                                           ', end='\r')
        if a == '':
            interval = (interval[1], interval[1]+INTERVAL_LEN)

        if a.lower() == 'q':
            exit(0)

        try: a = int(a)
        except: 0

    print('Downloading your video!')
    system(f"youtube-dl -i \"{videos[a]['url']}\"")


if __name__ == "__main__":
    main()
